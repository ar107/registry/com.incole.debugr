﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public static class Log
{
    static Dictionary<int, byte[]> _list;
    static byte[][] _colors;
    static int _index;

    static Log()
    {
        _list = new Dictionary<int, byte[]>();
        _colors = new byte[][]
        {
            new byte[] {230, 25, 75 },
            new byte[] {60, 180, 75},
            new byte[] {255, 225, 25},
            new byte[] {0, 130, 200},
            new byte[] {245, 130, 48},
            new byte[] {145, 30, 180},
            new byte[] {70, 240, 240},
            new byte[] {240, 50, 230},
            new byte[] {210, 245, 60},
            new byte[] {250, 190, 212},
            new byte[] {0, 128, 128},
            new byte[] {220, 190, 255},
            new byte[] {170, 110, 40},
            new byte[] {255, 250, 200},
            new byte[] {128, 0, 0},
            new byte[] {170, 255, 195},
            new byte[] {128, 128, 0},
            new byte[] {255, 215, 180},
            new byte[] {0, 0, 128},
            new byte[] {128, 128, 128},
            new byte[] {255, 255, 255}
        };
        _index = 0;
    }

    private static byte[] Setup(Type type)
    {
        var hash = type.GetHashCode();

        if (!_list.ContainsKey(hash))
        {
            _list.Add(hash, _colors[_index]);
            _index++;

            if (_index > _colors.Length - 1)
                _index = 0;
        }

        return _list[hash];
    }

    public static void LogInfo<T>(this T initiator, string message) where T: class
    {
        var type = initiator.GetType();
        byte[] color = Setup(type);

        Debug.Log($"<size=10><color=#{color[0]:X2}{color[1]:X2}{color[2]:X2}>[{type.Name}]</color></size> {message}", initiator as Object);
    }


    public static void LogWarning<T>(this T initiator, string message) where T: class
    {
        var type = initiator.GetType();
        byte[] color = Setup(type);

        Debug.LogWarning($"<size=10><color=#{color[0]:X2}{color[1]:X2}{color[2]:X2}>[{type.Name}]</color></size> {message}", initiator as Object);
    }
    
    public static void LogWarning<T>(this T initiator, string message, Object target) where T: class
    {
        var type = initiator.GetType();
        byte[] color = Setup(type);

        Debug.LogWarning($"<size=10><color=#{color[0]:X2}{color[1]:X2}{color[2]:X2}>[{type.Name}]</color></size> {message}", target);
    }

    public static void LogError<T>(this T initiator, string message) where T: class
    {
        var type = initiator.GetType();
        byte[] color = Setup(type);

        Debug.LogError($"<size=10><color=#{color[0]:X2}{color[1]:X2}{color[2]:X2}>[{type.Name}]</color></size> {message}", initiator as Object);
    }
    
    public static void LogError<T>(this T initiator, string message, Object target) where T: class
    {
        var type = initiator.GetType();
        byte[] color = Setup(type);

        Debug.LogError($"<size=10><color=#{color[0]:X2}{color[1]:X2}{color[2]:X2}>[{type.Name}]</color></size> {message}", target);
    }

    
    public static void LogError(Type type, string message)
    {
        byte[] color = Setup(type);

        Debug.LogError($"<size=10><color=#{color[0]:X2}{color[1]:X2}{color[2]:X2}>[{type.Name}]</color></size> {message}");
    }
}
